from kt2.graaf import Graaf, Tipp, Serv
from kt2.kruskali import Kruskali

a = Tipp("a")
b = Tipp("b")
c = Tipp("c")
d = Tipp("d")
e = Tipp("e")
f = Tipp("f")
g = Tipp("g")
h = Tipp("h")

graaf = Graaf()
graaf += Serv(a, b)
graaf += Serv(a, c)
graaf += Serv(a, d)
graaf += Serv(b, c)
graaf += Serv(d, c)
graaf += Serv(b, e)
graaf += Serv(c, e)
graaf += Serv(f, c)
graaf += Serv(d, f)
graaf += Serv(d, g)
graaf += Serv(e, h)
graaf += Serv(h, f)
graaf += Serv(g, h)

graaf = Kruskali(graaf)
toes = graaf.toesepuu()

toes.joonista()
graaf.joonista()
