from queue import PriorityQueue

from kt2.graaf import Graaf, KaugusegaTipp


class Dijkstra(Graaf):
    def __new__(cls, *args, **kwargs):
        if len(args) != 0 and isinstance(args[0], Graaf):
            args[0].tipud = list(map(lambda tipp: KaugusegaTipp(tipp), args[0].tipud))
            args[0].__class__ = Dijkstra
            return args[0]
        else:
            return super(Dijkstra, cls).__new__(cls)

    def läbi(self, start, finish=None):
        # Kui eelistusjärjekord realiseerida kuhjana, on esitatud
        # variandi keerukus O(n + m log m), kus n ja m on vastavalt
        # graafi tippude ja kaarte arv.

        start.kaugus = 0
        eelisjärjekord = PriorityQueue()
        eelisjärjekord.put(start)
        leitud = {start}

        while not eelisjärjekord.empty():
            vaadeldav = eelisjärjekord.get()
            if vaadeldav == finish:
                # Peale välja võtmist enam paremaks muutuda ei saa
                return vaadeldav.kaugus
            for serv in vaadeldav.algavad:
                if vaadeldav.kaugus + serv.pikkus < serv.teine(vaadeldav).kaugus:
                    # Parandame kaugust
                    serv.teine(vaadeldav).kaugus = vaadeldav.kaugus + serv.pikkus
                # Kui tipp on juba järjekorrast välja võetud, siis enam tagasi ei pane
                if serv.teine(vaadeldav) not in leitud:
                    eelisjärjekord.put(serv.teine(vaadeldav))
                    leitud.add(serv.teine(vaadeldav))
