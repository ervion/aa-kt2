from kt2.graaf import KaugusegaTipp, Graaf


class BellmannFordi(Graaf):
    def __new__(cls, *args, **kwargs):
        if len(args) != 0 and isinstance(args[0], Graaf):
            args[0].tipud = list(map(lambda tipp: KaugusegaTipp(tipp), args[0].tipud))
            args[0].__class__ = BellmannFordi
            return args[0]
        else:
            return super(BellmannFordi, cls).__new__(cls)

    def läbi(self, algus):
        algus.kaugus = 0
        for t in range(len(self.tipud) - 2):
            for serv in self.servad:
                if serv.algus.kaugus + serv.pikkus < serv.lõpp.kaugus:
                    serv.lõpp.kaugus = serv.algus.kaugus + serv.pikkus
