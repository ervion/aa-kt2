import matplotlib.pyplot as plt
import networkx as nx

class Graaf:
    def __new__(cls, *args, **kwargs):
        if len(args) != 0 and isinstance(args[0], Graaf):
            return args[0]
        else:
            return super(Graaf, cls).__new__(cls)

    def __init__(self, tipud=None, servad=None):
        if not isinstance(tipud, Graaf):
            self.tipud = tipud if tipud else set()
            self.servad = servad if servad else set()

    def __add__(self, other):
        if isinstance(other, Tipp):
            self.tipud.add(other)
            for serv in other.algavad:
                self.servad.add(serv)
        elif isinstance(other, Serv):
            self.servad.add(other)
            for ots in other.otsad:
                self.tipud.add(ots)
        return self

    def joonista(self):
        G = nx.DiGraph()
        G.add_nodes_from(self.tipud)
        for serv in self.servad:
            for (algus, lõpp) in serv.suunad:
                G.add_edge(algus, lõpp, weight=serv.pikkus)

        edge_labels = dict([((u, v,), d['weight']) for u, v, d in G.edges(data=True)])
        pos = nx.nx_pydot.graphviz_layout(G)
        # pos = nx.spring_layout(G)
        nx.draw_networkx_edge_labels(G, pos, edge_labels=edge_labels)
        nx.draw(G, pos, with_labels=True, node_color='w', node_size=1500)
        plt.show()

    def find(self, väärtus):
        return next(tipp for tipp in self.tipud if tipp.väärtus == väärtus)


class Tipp:
    def __new__(cls, *args, **kwargs):
        if len(args) != 0 and isinstance(args[0], Tipp):
            return args[0]
        else:
            return super(Tipp, cls).__new__(cls)

    def __init__(self, väärtus):
        if not isinstance(väärtus, Tipp):
            self.väärtus = väärtus
            self.algavad = set()
            self.lõppevad = set()

    def __add__(self, other):
        return Serv(self, other)

    def __lt__(self, other):
        return str(self.väärtus) < str(other.väärtus)

    def __str__(self):
        return str(self.väärtus)

    def __eq__(self, other):
        if isinstance(other, Tipp):
            return self.väärtus == other.väärtus
        else:
            return self.väärtus == other

    def __ne__(self, other):
        return not self.__eq__(other)

    def __hash__(self):
        return hash(self.väärtus)


class KaugusegaTipp(Tipp):
    def __new__(cls, *args, **kwargs):
        if len(args) != 0 and isinstance(args[0], Tipp):
            args[0].kaugus = float("inf")
            args[0].__class__ = KaugusegaTipp
            return args[0]
        else:
            return super(KaugusegaTipp, cls).__new__(cls)

    def __init__(self, väärtus, kaugus=float("inf")):
        super().__init__(väärtus)
        self.kaugus = kaugus

    def __lt__(self, other):
        return self.kaugus < other.kaugus

    def __str__(self):
        return super().__str__() + " (" + str(self.kaugus) + ")"


class Serv:
    def __new__(cls, *args, **kwargs):
        if len(args) != 0 and isinstance(args[0], Serv):
            args[0].otsad = {args[0].algus, args[0].lõpp}
            args[0].algus.algavad.add(args[0])
            args[0].lõpp.algavad.add(args[0])
            args[0].algus.lõppevad.add(args[0])
            args[0].lõpp.lõppevad.add(args[0])
            return args[0]
        else:
            return super(Serv, cls).__new__(cls)

    def __init__(self, ots1, ots2=None, pikkus=0, kaar=False):
        if not isinstance(ots1, Serv) and not kaar:
            self.otsad = {ots1, ots2}
            self.pikkus = pikkus
            ots1.algavad.add(self)
            ots2.algavad.add(self)
            ots1.lõppevad.add(self)
            ots2.lõppevad.add(self)

    def __add__(self, other):
        self.pikkus += other
        return self

    def __eq__(self, other):
        if isinstance(other, Serv):
            return self.otsad == other.otsad and self.pikkus == other.pikkus
        else:
            return False

    def __ne__(self, other):
        return not self.__eq__(other)

    def __hash__(self):
        return hash((self.algus, self.lõpp, self.pikkus))

    @property
    def suunad(self):
        return [
            (self.algus, self.lõpp),
            (self.lõpp, self.algus)
        ]

    def teine(self, algus):
        for ots in self.otsad:
            if ots != algus:
                return ots

    @property
    def algus(self):
        return min(self.otsad)

    @property
    def lõpp(self):
        return self.teine(self.algus)

    def __lt__(self, other):
        return self.pikkus < other.pikkus


class Kaar(Serv):
    def __init__(self, ots1, ots2, pikkus=0):
        super().__init__(ots1, ots2, pikkus, kaar=True)
        self.otsad = [ots1, ots2]
        self.pikkus = pikkus
        ots1.algavad.add(self)
        ots2.lõppevad.add(self)

    @property
    def lõpp(self):
        return self.otsad[1]

    @property
    def algus(self):
        return self.otsad[0]

    @property
    def suunad(self):
        return [
            (self.algus, self.lõpp)
        ]

