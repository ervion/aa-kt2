from kt2.graaf import Graaf


class FloydWarshalli(Graaf):
    def __new__(cls, *args, **kwargs):
        if len(args) != 0 and isinstance(args[0], Graaf):
            args[0].külgnevusmaatriks = {}
            for tipp1 in args[0].tipud:
                args[0].külgnevusmaatriks[tipp1] = {}
                for tipp2 in args[0].tipud:
                    if tipp2 == tipp1:
                        args[0].külgnevusmaatriks[tipp1][tipp2] = 0
                    else:
                        args[0].külgnevusmaatriks[tipp1][tipp2] = float("inf")
            for serv in args[0].servad:
                for (algus, lõpp) in serv.suunad:
                    args[0].külgnevusmaatriks[algus][lõpp] = serv.pikkus
            args[0].kaugustemaatriks = args[0].külgnevusmaatriks
            args[0].__class__ = FloydWarshalli
            return args[0]
        else:
            return super(FloydWarshalli, cls).__new__(cls)

    def __init__(self, tipud=None, servad=None):
        if not isinstance(tipud, Graaf):
            super().__init__(tipud, servad)
            self.külgnevusmaatriks = {}
            self.kaugustemaatriks = {}

    def läbi(self):
        m = self.külgnevusmaatriks
        for t in self.tipud:
            for i in self.tipud:
                for j in self.tipud:
                    if m[i][t] + m[t][j] < m[i][j]:
                        m[i][j] = m[i][t] + m[t][j]
        self.kaugustemaatriks = m

    def joonista(self):
        print(str("").rjust(5), end=" ")
        for tipp in self.tipud:
            print(str(tipp).rjust(5), end=" ")
        print()
        for tipp1 in self.tipud:
            print(str(tipp1).rjust(5), end=" ")
            for tipp2 in self.tipud:
                print(str(self.kaugustemaatriks[tipp1][tipp2]).rjust(5), end=" ")
            print()
        super().joonista()
