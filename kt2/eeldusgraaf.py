from kt2.graaf import Tipp, Graaf
from kt2.kahni import Kahni


class EeldusGraaf(Graaf):
    def __new__(cls, *args, **kwargs):
        if len(args) != 0 and isinstance(args[0], Graaf):
            args[0].tipud = list(map(lambda tipp: EeldusTipp(tipp), args[0].tipud))
            args[0].__class__ = EeldusGraaf
            return args[0]
        else:
            return super(EeldusGraaf, cls).__new__(cls)

    def leiaVaraseimadLõpud(self):
        for tipp in Kahni(self).topoloogiliselt:
            tipp = EeldusTipp(tipp)
            tipp.varaseimlõpp = tipp.kaal
            for serv in tipp.lõppevad:
                if serv.teine(tipp).varaseimlõpp + tipp.kaal > tipp.varaseimlõpp:
                    tipp.varaseimlõpp = serv.teine(tipp).varaseimlõpp + tipp.kaal
        EeldusGraaf(self)

    @property
    def varaseimLõpp(self):
        temp = 0
        for tipp in self.tipud:
            if tipp.varaseimlõpp > temp:
                temp = tipp.varaseimlõpp
        return temp

    def leiaHiliseimadAlgused(self):
        for tipp in Kahni(self).topoloogiliselt[::-1]:
            tipp = EeldusTipp(tipp)
            tipp.hiliseimalgus = EeldusGraaf(self).varaseimLõpp - tipp.kaal
            for serv in tipp.algavad:
                if serv.teine(tipp).hiliseimalgus - tipp.kaal < tipp.hiliseimalgus:
                    tipp.hiliseimalgus = serv.teine(tipp).hiliseimalgus - tipp.kaal


class EeldusTipp(Tipp):
    def __new__(cls, *args, **kwargs):
        if len(args) != 0 and isinstance(args[0], Tipp):
            args[0].__class__ = EeldusTipp
            return args[0]
        else:
            return super(EeldusTipp, cls).__new__(cls)

    def __init__(self, väärtus):
        super().__init__(väärtus)
        if not hasattr(self, "kaal"):
            self.kaal = 10
        if not hasattr(self, "varaseimlõpp"):
            self.varaseimlõpp = float("inf")
        if not hasattr(self, "hiliseimalgus"):
            self.hiliseimalgus = 0

    def __str__(self):
        return super().__str__() + "(" + str(self.kaal) + ")\n" + str(self.hiliseimalgus) + "-" + str(self.varaseimlõpp)
