from queue import Queue

from kt2.graaf import Tipp, Graaf


class Kahni(Graaf):
    def __new__(cls, *args, **kwargs):
        if len(args) != 0 and isinstance(args[0], Graaf):
            args[0].tipud = list(map(lambda tipp: KahniTipp(tipp), args[0].tipud))
            args[0].__class__ = Kahni
            return args[0]
        else:
            return super(Kahni, cls).__new__(cls)

    @property
    def topoloogiliselt(self):
        Q = Queue()
        for tipp in self.tipud:
            if tipp.kahni == 0:
                Q.put(tipp)
        topo = []
        while not Q.empty():
            võetav = Q.get()
            topo.append(võetav)
            for serv in võetav.algavad:
                serv.teine(võetav).kahni -= 1
                if serv.teine(võetav).kahni == 0:
                    Q.put(serv.teine(võetav))
        return topo


class KahniTipp(Tipp):
    def __new__(cls, *args, **kwargs):
        if len(args) != 0 and isinstance(args[0], Tipp):
            args[0].__class__ = KahniTipp
            return args[0]
        else:
            return super(KahniTipp, cls).__new__(cls)

    def __init__(self, väärtus):
        super().__init__(väärtus)
        self.kahni = len(self.lõppevad)
