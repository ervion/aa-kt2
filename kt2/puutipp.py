from kt2.graaf import Tipp, Serv, Kaar


class PuuTipp(Tipp):
    def __init__(self, väärtus, leht=False):
        super().__init__(väärtus)
        if leht:
            self.servad = []
        else:
            self.servad = [Serv(self, Leht()), Serv(self, Leht())]

    def __add__(self, other):
        return Kaar(self, other)

    @property
    def vasak(self):
        return self.servad[0]

    @vasak.setter
    def vasak(self, alampuu):
        self.servad[0] = self + alampuu

    @property
    def parem(self):
        return self.servad[1]

    @parem.setter
    def parem(self, alampuu):
        self.servad[1] = self + alampuu


class Leht(PuuTipp):
    def __init__(self):
        super().__init__("", leht=True)

    def __str__(self, depth=0):
        return ""

    @property
    def parem(self):
        return Serv(self, Leht())

    @property
    def vasak(self):
        return Serv(self, Leht())
