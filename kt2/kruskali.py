from queue import Queue

from kt2.graaf import Graaf, Serv


class Kruskali(Graaf):
    def __new__(cls, *args, **kwargs):
        if len(args) != 0 and isinstance(args[0], Graaf):
            args[0].servad = list(map(lambda serv: Serv(serv), args[0].servad))
            args[0].__class__ = Kruskali
            return args[0]
        else:
            return super(Kruskali, cls).__new__(cls)

    def ühenduses(self, algus, lõpp):
        q = Queue()
        q.put(algus)
        vaadeldud = {algus}
        while not q.empty():
            vaadeldav = q.get()
            vaadeldud.add(vaadeldav)
            if vaadeldav == lõpp:
                return True
            for serv in vaadeldav.algavad:
                if serv in self.servad and serv.teine(vaadeldav) not in vaadeldud:
                    q.put(serv.teine(vaadeldav))
        return False

    def toesepuu(self):
        toes = Kruskali(self.tipud)
        jrk = sorted(self.servad)
        for serv in jrk:
            if not toes.ühenduses(serv.algus, serv.lõpp):
                toes += serv
        return toes
