from queue import PriorityQueue

from kt2.graaf import Graaf, Serv


class Primi(Graaf):
    def __new__(cls, *args, **kwargs):
        if len(args) != 0 and isinstance(args[0], Graaf):
            args[0].servad = list(map(lambda serv: Serv(serv), args[0].servad))
            args[0].__class__ = Primi
            return args[0]
        else:
            return super(Primi, cls).__new__(cls)

    def toesepuu(self, algus):
        toes = Graaf()
        q = PriorityQueue()
        q.put(algus)
        while not q.empty():
            vaadeldav = q.get()
            for serv in vaadeldav.algavad:
                if not (serv.algus in toes.tipud and serv.lõpp in toes.tipud):
                    toes += serv
                    q.put(serv.teine(vaadeldav))
        return toes
