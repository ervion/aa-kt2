from kt2.graaf import Graaf, Tipp


class Kosaraju(Graaf):
    def __new__(cls, *args, **kwargs):
        if len(args) != 0 and isinstance(args[0], Graaf):
            args[0].tipud = list(map(lambda tipp: KlassigaTipp(tipp), args[0].tipud))
            args[0].__class__ = Kosaraju
            return args[0]
        else:
            return super(Kosaraju, cls).__new__(cls)

    def tagurpidilõppjärjestus(self, algus):
        järjestus, läbitud = algus.lõppjärjestuses([], set())
        return järjestus

    def jaota(self, algus):
        for tipp in self.tagurpidilõppjärjestus(algus):
            if tipp.klass is None:
                tipp.klass = tipp


class KlassigaTipp(Tipp):
    def __new__(cls, *args, **kwargs):
        if len(args) != 0 and isinstance(args[0], Tipp):
            args[0].__class__ = KlassigaTipp
            args[0]._klass = None
            return args[0]
        else:
            return super(KlassigaTipp, cls).__new__(cls)

    def __init__(self, väärtus):
        if not isinstance(väärtus, Tipp):
            super(KlassigaTipp, self).__init__(väärtus)
            self._klass = None

    @property
    def klass(self):
        return self._klass

    @klass.setter
    def klass(self, klass):
        self._klass = klass
        for serv in self.lõppevad:
            if serv.teine(self).klass is None:
                serv.teine(self).klass = klass

    def lõppjärjestuses(self, poolik, läbitud):
        läbitud.add(self)
        for serv in self.algavad:
            if serv.teine(self) not in läbitud:
                poolik, läbitud = serv.teine(self).lõppjärjestuses(poolik, läbitud)
        poolik.insert(0, self)
        return poolik, läbitud

    def __str__(self):
        return super().__str__() + "(" + ("-" if self._klass is None else str(self._klass.väärtus)) + ")"
